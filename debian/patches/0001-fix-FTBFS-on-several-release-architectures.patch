Description: fix FTBFS on several release architectures
Origin: backport, https://github.com/biojppm/debugbreak/commit/8b4a755e76717103adc814c0c05ceb3b91befa7d
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1013310
Last-Update: 2022-06-30

---
 .../ext/c4core/src/c4/ext/debugbreak/debugbreak.h  | 83 +++++++++++-----------
 1 file changed, 43 insertions(+), 40 deletions(-)

diff --git a/third_party/rapidyaml/rapidyaml/ext/c4core/src/c4/ext/debugbreak/debugbreak.h b/third_party/rapidyaml/rapidyaml/ext/c4core/src/c4/ext/debugbreak/debugbreak.h
index f570bf8..e91121d 100644
--- a/third_party/rapidyaml/rapidyaml/ext/c4core/src/c4/ext/debugbreak/debugbreak.h
+++ b/third_party/rapidyaml/rapidyaml/ext/c4core/src/c4/ext/debugbreak/debugbreak.h
@@ -1,4 +1,4 @@
-/* Copyright (c) 2011-2015, Scott Tsai
+/* Copyright (c) 2011-2018, Scott Tsai
  * 
  * All rights reserved.
  * 
@@ -23,9 +23,7 @@
  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  * POSSIBILITY OF SUCH DAMAGE.
  */
-
 #ifndef DEBUG_BREAK_H
-#define DEBUG_BREAK_H
 
 #ifdef _MSC_VER
 
@@ -33,27 +31,22 @@
 
 #else
 
-#include <signal.h>
-
 #ifdef __cplusplus
 extern "C" {
 #endif
 
-enum {
-	/* gcc optimizers consider code after __builtin_trap() dead.
-	 * Making __builtin_trap() unsuitable for breaking into the debugger */
-	DEBUG_BREAK_PREFER_BUILTIN_TRAP_TO_SIGTRAP = 0,
-};
+#define DEBUG_BREAK_USE_TRAP_INSTRUCTION 1
+#define DEBUG_BREAK_USE_BULTIN_TRAP      2
+#define DEBUG_BREAK_USE_SIGTRAP          3
 
 #if defined(__i386__) || defined(__x86_64__)
-enum { HAVE_TRAP_INSTRUCTION = 1, };
-__attribute__((gnu_inline, always_inline))
+	#define DEBUG_BREAK_IMPL DEBUG_BREAK_USE_TRAP_INSTRUCTION
 __inline__ static void trap_instruction(void)
 {
 	__asm__ volatile("int $0x03");
 }
 #elif defined(__thumb__)
-enum { HAVE_TRAP_INSTRUCTION = 1, };
+	#define DEBUG_BREAK_IMPL DEBUG_BREAK_USE_TRAP_INSTRUCTION
 /* FIXME: handle __THUMB_INTERWORK__ */
 __attribute__((gnu_inline, always_inline))
 __inline__ static void trap_instruction(void)
@@ -69,30 +62,35 @@ __inline__ static void trap_instruction(void)
 #endif
 
 	/* Known problem:
-	 * After a breakpoint hit, can't stepi, step, or continue in GDB.
-	 * 'step' stuck on the same instruction.
+	 * After a breakpoint hit, can't 'stepi', 'step', or 'continue' in GDB.
+	 * 'step' would keep getting stuck on the same instruction.
+	 *
+	 * Workaround: use the new GDB commands 'debugbreak-step' and
+	 * 'debugbreak-continue' that become available
+	 * after you source the script from GDB:
 	 *
-	 * Workaround: a new GDB command,
-	 * 'debugbreak-step' is defined in debugbreak-gdb.py
-	 * that does:
+	 * $ gdb -x debugbreak-gdb.py <... USUAL ARGUMENTS ...>
+	 *
+	 * 'debugbreak-step' would jump over the breakpoint instruction with
+	 * roughly equivalent of:
 	 * (gdb) set $instruction_len = 2
 	 * (gdb) tbreak *($pc + $instruction_len)
 	 * (gdb) jump   *($pc + $instruction_len)
 	 */
 }
 #elif defined(__arm__) && !defined(__thumb__)
-enum { HAVE_TRAP_INSTRUCTION = 1, };
+	#define DEBUG_BREAK_IMPL DEBUG_BREAK_USE_TRAP_INSTRUCTION
 __attribute__((gnu_inline, always_inline))
 __inline__ static void trap_instruction(void)
 {
 	/* See 'arm-linux-tdep.c' in GDB source,
 	 * 'eabi_linux_arm_le_breakpoint' */
 	__asm__ volatile(".inst 0xe7f001f0");
-	/* Has same known problem and workaround
-	 * as Thumb mode */
+	/* Known problem:
+	 * Same problem and workaround as Thumb mode */
 }
 #elif defined(__aarch64__)
-enum { HAVE_TRAP_INSTRUCTION = 1, };
+	#define DEBUG_BREAK_IMPL DEBUG_BREAK_USE_TRAP_INSTRUCTION
 __attribute__((gnu_inline, always_inline))
 __inline__ static void trap_instruction(void)
 {
@@ -101,34 +99,39 @@ __inline__ static void trap_instruction(void)
 	__asm__ volatile(".inst 0xd4200000");
 }
 #else
-enum { HAVE_TRAP_INSTRUCTION = 0, };
+	#define DEBUG_BREAK_IMPL DEBUG_BREAK_USE_SIGTRAP
 #endif
 
+
+#ifndef DEBUG_BREAK_IMPL
+#error "debugbreak.h is not supported on this target"
+#elif DEBUG_BREAK_IMPL == DEBUG_BREAK_USE_TRAP_INSTRUCTION
+__attribute__((gnu_inline, always_inline))
+__inline__ static void debug_break(void)
+{
+	trap_instruction();
+}
+#elif DEBUG_BREAK_IMPL == DEBUG_BREAK_USE_BULTIN_TRAP
+__attribute__((gnu_inline, always_inline))
+__inline__ static void debug_break(void)
+{
+	__builtin_trap();
+}
+#elif DEBUG_BREAK_IMPL == DEBUG_BREAK_USE_SIGTRAP
+#include <signal.h>
 __attribute__((gnu_inline, always_inline))
 __inline__ static void debug_break(void)
 {
-	if (HAVE_TRAP_INSTRUCTION) {
-		trap_instruction();
-	} else if (DEBUG_BREAK_PREFER_BUILTIN_TRAP_TO_SIGTRAP) {
-		 /* raises SIGILL on Linux x86{,-64}, to continue in gdb:
-		  * (gdb) handle SIGILL stop nopass
-		  * */
-		__builtin_trap();
-	} else {
-		#ifdef _WIN32
-		/* SIGTRAP available only on POSIX-compliant operating systems
-		 * use builtin trap instead */
-		__builtin_trap();
-		#else
-		raise(SIGTRAP);
-		#endif
-	}
+	raise(SIGTRAP);
 }
+#else
+#error "invalid DEBUG_BREAK_IMPL value"
+#endif
 
 #ifdef __cplusplus
 }
 #endif
 
-#endif
+#endif /* ifdef _MSC_VER */
 
 #endif
